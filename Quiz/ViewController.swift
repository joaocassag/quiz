//
//  ViewController.swift
//  Quiz
//
//  Created by Joao Cassamano_old on 3/27/20.
//  Copyright © 2020 Joao Cassamano. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var currentQuestionLabel: UILabel!
    @IBOutlet var currentQuestionLabelCenterXConstraint: NSLayoutConstraint!
    @IBOutlet var nextQuestionLabel: UILabel!
    @IBOutlet var nextQUestionLabelCenterXConstraint: NSLayoutConstraint!
    @IBOutlet var answerLabel: UILabel!
    
    let questions: [String] = [
        "What is 7+7",
        "What is the capitatl of Vermont?",
        "Wha is cognac made from?"
    ]
    let answers: [String] = [
        "14",
        "Montpelier",
        "Grapes"
    ]
    var currentQuestionIndex: Int = 0
    
    @IBAction func showNextQuestion(_ sender: UIButton){
        print("###entered showNextQuestion")
        currentQuestionIndex+=1
        if currentQuestionIndex == questions.count{
            currentQuestionIndex=0
        }
        let question: String = questions[currentQuestionIndex]
        nextQuestionLabel.text = question
        answerLabel.text="???"
        
        animateLabelTransitions()
    }
    
    @IBAction func showAnswer(_ sender: UIButton){
        print("###entered showAnswer")
        let answer: String = answers[currentQuestionIndex]
        answerLabel.text = answer
    }
    
    
    override func viewDidLoad() {
        print("entered viewDidLoad")
        super.viewDidLoad()
        currentQuestionLabel.text = questions[currentQuestionIndex]
        
        updateOffScreenLabel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("###entered vieWillAppear")
        super.viewWillAppear(animated)
        
        nextQuestionLabel.alpha = 0
    }
    
    func animateLabelTransitions(){
        print("###entered after closure declaration")
        view.layoutIfNeeded()
        
        let screenWidth = view.frame.width
        self.nextQUestionLabelCenterXConstraint.constant = 0
        self.currentQuestionLabelCenterXConstraint.constant += screenWidth
        
        UIView.animate(withDuration: 0.5,
           delay: 0,
           options: [.curveLinear],
           animations: {
                self.currentQuestionLabel.alpha = 0
                self.nextQuestionLabel.alpha = 1
            
            self.view.layoutIfNeeded()
            },
           completion: { _ in
                swap(&self.currentQuestionLabel, &self.nextQuestionLabel)
                swap(&self.currentQuestionLabelCenterXConstraint,&self.nextQUestionLabelCenterXConstraint)
            
                self.updateOffScreenLabel()
        })
        print("###entered after animate call")
    }
    
    func updateOffScreenLabel(){
        let screenWidth = view.frame.width
        nextQUestionLabelCenterXConstraint.constant = -screenWidth
    }
    
}

